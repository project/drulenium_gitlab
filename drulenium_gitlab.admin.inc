<?php

function drulenium_gitlab_settings_form($form, &$form_state) {
  $form['gitlab_url'] = array(
    '#title' => t('URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('gitlab_url', 'https://gitlab.com/api/v3/'),
    '#description' => t('Example: https://gitlab.com/api/v3/'),
    '#required' => TRUE,
  );

  $form['gitlab_token'] = array(
    '#title' => t('Private token'),
    '#type' => 'textfield',
    '#default_value' => variable_get('gitlab_token', ''),
    '#required' => TRUE,
  );

  $form['submit_initialize'] = array(
      '#type' => 'submit',
      '#weight' => 100,
      '#value' => t('Save & Initialize'),
      '#submit' => array('drulenium_gitlab_settings_form_submit'),
  );
  $form = system_settings_form($form);

  //$form['#submit'][] = 'drulenium_gitlab_settings_form_submit';
  return $form;
}

function drulenium_gitlab_settings_form_submit($form, &$form_state) {
  variable_set("gitlab_url", $form_state['values']['gitlab_url']);
  variable_set("gitlab_token", $form_state['values']['gitlab_token']);

  drulenium_gitlab_initialize();
}
